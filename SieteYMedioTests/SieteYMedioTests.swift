//
//  SieteYMedioTests.swift
//  SieteYMedioTests
//
//  Created by Master Móviles on 5/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import XCTest

@testable import SieteYMedio

class SieteYMedioTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCarta() {
        let carta = Carta(3, de: Palo.copas)
        
        XCTAssertEqual(carta?.valor, 3)
        XCTAssertEqual(carta?.palo, Palo.copas)
    }
    
    func testBaraja() {
        let baraja = Baraja()
        
        XCTAssertEqual(baraja.cartas.count, 40)
    }
    
    func testQuitarDeBaraja() {
        let baraja = Baraja()
        
        let cartaEliminada = baraja.repartirCarta()
        
        XCTAssertEqual(baraja.cartas.count, 39)
        XCTAssertFalse(baraja.cartas.contains(where: {carta in return carta.descripcion == cartaEliminada.descripcion}))
    }
    
}
