//
//  Mano.swift
//  SieteYMedio
//
//  Created by Master Móviles on 5/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import Foundation

class Mano {
    var cartas = [Carta]()
    
    func addCarta(_ carta: Carta) {
        cartas.append(carta)
    }
    
    func sumarMano() -> Float {
        var total: Float = 0.0
        cartas.forEach() {
            carta in
            let valor = ((carta.valor <= 7) ? Float(carta.valor) : 0.5)
            total += valor
        }
        return total
    }
}
