//
//  ViewController.swift
//  SieteYMedio
//
//  Created by Master Móviles on 5/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var nuevoJuegoButton: UIButton!
    @IBOutlet weak var robarCartaButton: UIButton!
    @IBOutlet weak var plantarseButton: UIButton!
    @IBOutlet weak var victoriasLabel: UILabel!
    
    var juego : JuegoSieteyMedio!
    var vistasCartas = [UIImageView]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        juego = JuegoSieteyMedio()
        
        toggleButtons()
        updateVictories()
    }
    
    func toggleButtons() {
        robarCartaButton.isEnabled = !(juego?.isEnded)!
        plantarseButton.isEnabled = !(juego?.isEnded)!
        nuevoJuegoButton.isEnabled = (juego?.isEnded)!
    }
    
    func updateVictories() {
        victoriasLabel.text = "Victorias: \(juego.victorias)\n Derrotas: \(juego.derrotas)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func repartirCarta(carta: Carta, enPosicion : Int) {
        let nombreImagen = String(carta.valor)+String(carta.palo.rawValue)
        //creamos un objeto imagen
        let imagenCarta = UIImage(named: nombreImagen)
        //para que la imagen sea un componente más del UI,
        //la encapsulamos en un UIImageView
        let cartaView = UIImageView(image: imagenCarta)
        //Inicialmente la colocamos fuera de la pantalla y más grande
        //para que parezca más cercana
        //"frame" son los límites de la vista, definen pos y tamaño
        cartaView.frame = CGRect(x: -200, y: -200, width: 200, height: 300)
        //La rotamos, para que luego al repartirla, gir
        cartaView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
        //La añadimos a la vista principal, si no no sería visible
        self.view.addSubview(cartaView)
        //guardamos la pista en el array, para luego poder eliminarla
        self.vistasCartas.append(cartaView)
        //Animación de repartir carta
        UIView.animate(withDuration: 0.5){
            //"efecto caida": la llevamos a la posición final
            cartaView.frame = CGRect(x:20+50*(enPosicion-1), y:100, width:70, height:100);
            //0 como ángulo "destino", para que rote mientras "cae"
            cartaView.transform = CGAffineTransform(rotationAngle:0);
        }
    }
    
    func repartirCartaMaquina(carta: Carta, enPosicion : Int) {
        let nombreImagen = String(carta.valor)+String(carta.palo.rawValue)
        //creamos un objeto imagen
        let imagenCarta = UIImage(named: nombreImagen)
        //para que la imagen sea un componente más del UI,
        //la encapsulamos en un UIImageView
        let cartaView = UIImageView(image: imagenCarta)
        //Inicialmente la colocamos fuera de la pantalla y más grande
        //para que parezca más cercana
        //"frame" son los límites de la vista, definen pos y tamaño
        cartaView.frame = CGRect(x: -200, y: -200, width: 200, height: 300)
        //La rotamos, para que luego al repartirla, gir
        cartaView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
        //La añadimos a la vista principal, si no no sería visible
        self.view.addSubview(cartaView)
        //guardamos la pista en el array, para luego poder eliminarla
        self.vistasCartas.append(cartaView)
        //Animación de repartir carta
        UIView.animate(withDuration: 0.5){
            //"efecto caida": la llevamos a la posición final
            cartaView.frame = CGRect(x:20+50*(enPosicion-1), y:250, width:70, height:100);
            //0 como ángulo "destino", para que rote mientras "cae"
            cartaView.transform = CGAffineTransform(rotationAngle:0);
        }
    }
    
    @IBAction func onNuevoJuegoClicked(_ sender: AnyObject) {
        juego?.nuevoJuego()
        toggleButtons()
        // Quitar cartas de la vista
        for carta in vistasCartas {
            carta.removeFromSuperview()
        }
        vistasCartas = []
    }

    @IBAction func onRobarCartaClicked(_ sender: AnyObject) {
        let (carta, fin) = (juego?.nuevaCarta())!
        if fin {
            toggleButtons()
        }
        print(carta.descripcion, fin)
        
        repartirCarta(carta: carta, enPosicion: juego.numMano!)
        if fin {
            let alert = UIAlertController(
                title: "Fin del juego",
                message: "¡¡Te has pasado!!",
                preferredStyle: UIAlertControllerStyle.alert)
            let action = UIAlertAction(
                title: "OK",
                style: UIAlertActionStyle.default)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
            updateVictories()
        }
    }
    
    @IBAction func onPlantarseClicked(_ sender: AnyObject) {
        let (puntos, fin) = (juego?.plantarse())!
        toggleButtons()
        
        var msg = ""
        if fin {
            msg = "¡¡Has Ganado!!"
        }
        else {
            msg = "¡¡Has Perdido!!"
        }
        
        let puntosMaquina: Float = self.juego.manoMaquina.sumarMano()
        msg += "Tienes \(puntos) y la máquina \(puntosMaquina)"
        
        var i = 0
        
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) {
            timer in
            if i < self.juego.maquina {
                i += 1
                self.repartirCartaMaquina(carta: self.juego.manoMaquina.cartas[i-1], enPosicion: i)
            } else {
                let alert = UIAlertController(
                    title: "Fin del juego",
                    message: msg,
                    preferredStyle: UIAlertControllerStyle.alert)
                let action = UIAlertAction(
                    title: "OK",
                    style: UIAlertActionStyle.default)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
                self.updateVictories()
                
                timer.invalidate()
            }
        }
        
        
        
        

        print("Plantarse con: \(puntos), la maquina tiene \(puntosMaquina). \(fin)")
    }
}

