//
//  Carta.swift
//  SieteYMedio
//
//  Created by Master Móviles on 5/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import Foundation

struct Carta {
    var palo : Palo
    var valor : Int
    var descripcion: String {
        get {
            return "el \(valor) de \(palo)"
        }
    }
    
    
    init?(_ valor: Int, de palo: Palo) {
        if (valor < 1 || valor > 12) {
            return nil
        } else {
            self.palo = palo
            self.valor = valor
        }
    }
}



/*
 
 // Terminar partida
 
 for vista in self.vistasCartas {
 
    vista.removeFromSuperview()
 }
 
 self.vistasCartas = []
 
 // Repartir cartas
 
 self.vistasCartas.append(cartaView) <- UIImageView con la carta
 
 */
