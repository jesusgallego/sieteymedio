//
//  Baraja.swift
//  SieteYMedio
//
//  Created by Master Móviles on 5/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import Foundation

class Baraja {
    var cartas = [Carta]()
    
    init() {
        for valor in 1...12 {
            if (valor != 8 && valor != 9) {
                cartas.append(Carta(valor, de: Palo.bastos)!)
                cartas.append(Carta(valor, de: Palo.oros)!)
                cartas.append(Carta(valor, de: Palo.copas)!)
                cartas.append(Carta(valor, de: Palo.espadas)!)
            }
        }
    }
    
    func repartirCarta() -> Carta {
        return cartas.popLast()!
    }
    
    func barajar() {
        cartas.shuffle()
    }
    
    
}
