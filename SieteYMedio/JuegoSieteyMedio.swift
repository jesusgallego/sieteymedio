//
//  JuegoSieteyMedio.swift
//  SieteYMedio
//
//  Created by Master Móviles on 5/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import Foundation

class JuegoSieteyMedio {
    var baraja: Baraja!
    var mano: Mano!
    var manoMaquina: Mano!
    var maquina: Int!
    var numMano: Int!
    var isEnded = false
    
    var victorias = 0
    var derrotas = 0
    
    let MIN_MACHINE: UInt32 = 2
    let MAX_MACHINE: UInt32 = 3
    
    let MAX: Float = 7.5
    
    init() {
        nuevoJuego()
    }
    
    func nuevoJuego() {
        print("Nuevo Juego")
        
        maquina = Int(arc4random_uniform(MAX_MACHINE) + MIN_MACHINE)
        print("La maquina tiene \(maquina) cartas")
        baraja = Baraja()
        mano = Mano()
        manoMaquina = Mano()
        
        numMano = 0
        isEnded = false
        
        baraja.barajar()
        
        for _ in 1...maquina {
            let carta = baraja.repartirCarta()
            print("añadir carta \(carta.descripcion)")
            manoMaquina.addCarta(carta)
        }
    }
    
    // True - puede seguir jugando
    // False - pierde
    func nuevaCarta() -> (Carta, Bool) {
        numMano = numMano + 1
        
        let carta = baraja.repartirCarta()
        mano.addCarta(carta)
        
        isEnded = mano.sumarMano() > MAX
        
        if isEnded {
            derrotas += 1
        }
        
        return (carta, isEnded)
    }
    
    // True - gana
    // False - pierde
    func plantarse() -> (Float, Bool) {
        isEnded = true
        let gana = manoMaquina.sumarMano() > MAX || mano.sumarMano() > manoMaquina.sumarMano()
        
        if gana {
            victorias += 1
        } else {
            derrotas += 1
        }
        
        return (mano.sumarMano(), gana)
    }
    
}
