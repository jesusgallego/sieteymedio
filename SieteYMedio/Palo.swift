//
//  Palo.swift
//  SieteYMedio
//
//  Created by Master Móviles on 5/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import Foundation

enum Palo : String {
    case oros, copas, bastos, espadas
}
